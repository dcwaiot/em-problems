# EM-problems

Problems and Solutions for electromagnetic problems.

### [Problem 1](prob1/prob1.md)
### [Problem 2](prob2/prob2.md)
### [Problem 3](prob3/prob3.md)
### [Problem 4](prob4/prob4.md)
### [Problem 5](prob5/prob5.md)
