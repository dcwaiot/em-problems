### [หน้าหลัก](../README.md)
### [โจทย์ปัญหา:](prob1TH.md)

โจทย์ปัญหาคือกำหนดให้ประจุแบบจุด $`Q`$ คูลอมบ์ อยู่ที่จุด $`(x_o, y_o, z_o)`$ เหนือระนาบตัวนำ เราจึงจำเป็นต้องประยุกต์ใช้ทฤษฎีจินตภำพ (image theory) โดยการแทนที่ระนาบตัวนำด้วยประจุจินตภาพของประจุแบบจุดที่โจทย์ให้มา ดังรูปที่แสดงด้านล่าง

<!--http://eng.sut.ac.th/tce/2016/administrator/ckfinder/userfiles/files/มคอ_%202%20หลักสูตรวิศวกรรมศาสตรมหาบัณฑิตTCE-CPE(1).pdf "image theory" = "ทฤษฎีจินตภำพ"-->

> We need image of the equivalent problem. Please take the file for the [orginal problem](/asset/goodnotes/Point-charge-image-theory-DCW.goodnotes) and modify it to the equivalent problem.

เมื่อใช้ทฤษฎี image theory,เราแทนที่ระนาบด้วยประจุ $`-Q`$ คูลอมบ์ ที่จุด $`(x_o, y_o, -z_o)`$.

สมการของสนามไฟฟ้าของจุด $`(x, y, z)`$ ผลิตจากจุดประจุ $`Q`$ คูลอมบ์ ที่จุด $`(x_o, y_o, z_o)`$ จะได้ตามสมการนี้
```math
\mathbf {E}^+ = \frac{Q}{4 \pi \varepsilon} \left( \frac {\mathbf {r-r'}}{|\mathbf {r-r'}|^3} \right)
``` 
เมื่อกำหนดให้ $` \mathbf r = x \mathbf a_x + y \mathbf a_y + z \mathbf a_z`$ and $` \mathbf r' = x_o \mathbf a_x + y_o \mathbf a_y + z_o \mathbf a_z`$ จะสามารถเขียนสมการได้ดังนี้

```math
\mathbf {E}^+ = \frac{Q}{4 \pi \varepsilon} \left( \frac {(x-x_o) \mathbf a_x + (y-y_o) \mathbf a_y + (z-z_o) \mathbf a_z}{\left( (x-x_o)^2+ (y-y_o)^2+ (z-z_o)^2 \right)^\frac{3}{2}}\right)
``` 

สมการของสนามเเม่ไฟฟ้าที่จุด $`(x, y, z)`$ ที่เกิดขึ่นโดยประจุ $`-Q`$ คูลอมบ์ ณ จุด $`(x_o, y_o, -z_o)`$ เป็นไปตามสมการด้านล่าง

```math
\mathbf {E}^- = \frac{-Q}{4 \pi \varepsilon} \left( \frac {(x-x_o) \mathbf a_x + (y-y_o) \mathbf a_y + (z+z_o) \mathbf a_z}{(x-x_o)^2+ (y-y_o)^2+ (z+z_o)^2)^\frac{3}{2}}\right)
``` 
สนามไฟฟ้ารวมคือ $`\mathbf E^+ + \mathbf E^-`$ สามารถเขียนได้ดังนี้
```math
\mathbf {E} = \frac{Q}{4 \pi \varepsilon} \left(\frac {(x-x_o) \mathbf a_x + (y-y_o) \mathbf a_y + (z-z_o) \mathbf a_z}{(x-x_o)^2+ (y-y_o)^2+ (z-z_o)^2)^\frac{3}{2}}-\frac {(x-x_o) \mathbf a_x + (y-y_o) \mathbf a_y + (z+z_o) \mathbf a_z}{(x-x_o)^2+ (y-y_o)^2+ (z+z_o)^2)^\frac{3}{2}}\right)
``` 
